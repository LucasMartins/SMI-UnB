#!/bin/bash

virtualenv -p python3 venv
source venv/bin/activate

pip3 install -r requirements.txt
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py loaddata src/smi_unb/fixtures/initial_data.json
python3 manage.py runserver 0.0.0.0:3000
