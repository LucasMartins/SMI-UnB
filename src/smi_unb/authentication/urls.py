from django.conf.urls import url

from . import views

app_name = 'authentication'

# FIXME: django 2.x have a better path definition
urlpatterns = [
    url(r'^$', views.make_login, name='make_login'),
    url(r'^logout/$', views.make_logout, name='make_logout'),
]
