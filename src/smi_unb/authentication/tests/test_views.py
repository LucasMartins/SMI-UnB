import mock

from django.contrib.auth.models import User
from django.urls import reverse
from django.test import Client, TestCase

from smi_unb.authentication.auth import EmailBackend


class TestUsersViews(TestCase):
    def setUp(self):
        self.client = Client()

        self.superuser = User(
            username='superuser',
            first_name='super',
            last_name='user',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

        self.normaluser = User(
            username='normaluser',
            first_name='normal',
            last_name='user',
            email="test@test.com",
            is_superuser=False,
        )
        self.normaluser.set_password('12345')
        self.normaluser.save()
    '''
    Login Tests
    '''
    def test_make_login(self):
        login_url = reverse('authentication:make_login')

        params = {
            'email': 'admin@admin.com',
            'password': '12345'
        }

        response = self.client.post(login_url, params)

        self.assertRedirects(response, reverse('dashboard'))

    def test_not_make_login_wrong_email(self):
        login_url = reverse('authentication:make_login')

        params = {
            'email': 'wrong_email@test.com',
            'password': '12345'
        }

        response = self.client.post(login_url, params)

        self.assertIn(
            'Email ou Senha Inválidos.',
            response.content.decode('utf-8')
        )

    def test_not_make_login_wrong_password(self):
        login_url = reverse('authentication:make_login')

        params = {
            'email': 'admin@admin.com',
            'password': 'wrongpassword'
        }

        response = self.client.post(login_url, params)

        self.assertIn(
            'Email ou Senha Inválidos.',
            response.content.decode('utf-8')
        )

    def test_login_and_redirects_to_correct_page(self):
        next_page = '?next=' + reverse('users:new_user')

        url = reverse('authentication:make_login') + next_page

        params = {
            'email': 'admin@admin.com',
            'password': '12345'
        }

        response = self.client.post(url, params)

        self.assertRedirects(response, reverse('users:new_user'))

    def test_user_authenticated_access_correct_next_page(self):
        self.client.login(username=self.superuser.email, password='12345')

        next_page = '?next=/usuarios/novo/'

        register_user_url = reverse('authentication:make_login') + next_page

        response = self.client.get(register_user_url)

        self.assertRedirects(response, reverse('users:new_user'))

    def test_user_authenticated_access_login_page(self):
        self.client.login(username=self.normaluser.email, password='12345')

        login_url = reverse('authentication:make_login')

        response = self.client.get(login_url)

        self.assertRedirects(response, reverse('dashboard'))

    def test_access_login_page(self):
        login_url = reverse('authentication:make_login')

        response = self.client.get(login_url)

        self.assertEqual(200, response.status_code)

    @mock.patch.object(
        EmailBackend, 'authenticate', return_value=None, autospec=True)
    def test_login_with_user_none_after_authentication(
        self, mocked_email_backend
    ):
        login_url = reverse('authentication:make_login')

        params = {
            'username': 'admin@admin.com',
            'password': '12345'
        }

        response = self.client.post(login_url, params)

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'users/login.html')

    def test_logout(self):
        self.client.login(username=self.superuser.email, password='12345')

        response_1 = self.client.get(reverse('dashboard'))
        self.assertEqual(200, response_1.status_code)

        logout_url = reverse('authentication:make_logout')
        logout_response = self.client.get(logout_url)
        self.assertEqual(302, logout_response.status_code)

        # User not logged in
        response_2 = self.client.get(reverse('dashboard'))
        self.assertEqual(302, response_2.status_code)
