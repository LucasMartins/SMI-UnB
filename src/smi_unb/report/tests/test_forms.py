from django.test import TestCase
from django.utils import timezone
from django.utils.translation import ugettext as _

from smi_unb.buildings.models import Building
from smi_unb.campuses.models import AdministrativeRegion, Campus
from smi_unb.transductor.models import TransductorModel, EnergyTransductor
from smi_unb.report.forms import NewGraphForm


class ReportFormTest(TestCase):
    def setUp(self):
        self.building = self.create_building()

        self.t_model = TransductorModel.objects.create(
            name="TR 4020",
            transport_protocol="UDP",
            serial_protocol="Modbus RTU",
            register_addresses=[[68, 0], [70, 1]],
        )

        self.transductor = EnergyTransductor.objects.create(
            building=self.building,
            model=self.t_model,
            name="transductor test",
            ip_address="111.111.111.111"
        )

    '''
    Correct Form Tests
    '''
    def test_new_graph_form_current_measurement(self):
        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': True,
            'past_measurement_day': '',
            'manual_initial_date': '',
            'manual_final_date': ''
        }

        form = NewGraphForm(data)

        self.assertTrue(form.is_valid())

    def test_new_graph_form_past_1_measurement_day(self):
        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': 1,
            'manual_initial_date': '',
            'manual_final_date': ''
        }

        form = NewGraphForm(data)

        self.assertTrue(form.is_valid())

    def test_new_graph_form_past_7_measurement_day(self):
        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': 7,
            'manual_initial_date': '',
            'manual_final_date': ''
        }

        form = NewGraphForm(data)

        self.assertTrue(form.is_valid())

    def test_new_graph_form_correct_manual_dates(self):
        now = timezone.now()
        later = timezone.now() + timezone.timedelta(days=7)

        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': '',
            'manual_initial_date': now,
            'manual_final_date': later
        }

        form = NewGraphForm(data)

        self.assertTrue(form.is_valid())

    '''
    Wrong Form Tests
    '''
    def test_new_graph_form_without_options(self):
        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': '',
            'manual_initial_date': '',
            'manual_final_date': ''
        }

        form = NewGraphForm(data)

        form_valid = form.is_valid()
        self.assertFalse(form_valid)

        self.assertEqual(
            [_('Nenhuma opção informada.')], form.non_field_errors())

    def test_new_graph_form_with_only_initial_date(self):
        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': '',
            'manual_initial_date': timezone.now(),
            'manual_final_date': ''
        }

        form = NewGraphForm(data)

        self.assertFalse(form.is_valid())

        self.assertEqual(
            [_('Campo de Data Final não foi informado.')],
            form.non_field_errors()
        )

    def test_new_graph_form_with_only_final_date(self):
        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': '',
            'manual_initial_date': '',
            'manual_final_date': timezone.now()
        }

        form = NewGraphForm(data)

        self.assertFalse(form.is_valid())

        self.assertEqual(
            [_('Campo de Data Inicial não foi informado.')],
            form.non_field_errors()
        )

    def test_new_graph_form_with_many_options(self):
        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': True,
            'past_measurement_day': '',
            'manual_initial_date': '',
            'manual_final_date': timezone.now()
        }

        form = NewGraphForm(data)

        self.assertFalse(form.is_valid())

        self.assertEqual(
            [_('Muitas opções escolhidas ao mesmo tempo.')],
            form.non_field_errors()
        )

        data['manual_final_date'] = ''
        data['past_measurement_day'] = 1

        self.assertFalse(form.is_valid())

        self.assertEqual(
            [_('Muitas opções escolhidas ao mesmo tempo.')],
            form.non_field_errors()
        )

    def test_new_graph_form_final_date_before_initial_date(self):
        now = timezone.now()
        previously = timezone.now() - timezone.timedelta(minutes=1)

        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': '',
            'manual_initial_date': now,
            'manual_final_date': previously
        }

        form = NewGraphForm(data)

        self.assertFalse(form.is_valid())

        self.assertEqual(
            [_('Data final não pode vir antes ou ser igual a data inicial.')],
            form.non_field_errors()
        )

    def test_new_graph_form_final_date_equal_initial_date(self):
        now = timezone.now()

        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': '',
            'manual_initial_date': now,
            'manual_final_date': now
        }

        form = NewGraphForm(data)

        self.assertFalse(form.is_valid())

        self.assertEqual(
            [_('Data final não pode vir antes ou ser igual a data inicial.')],
            form.non_field_errors()
        )

    def test_new_graph_form_date_too_long(self):
        max_graph_days = 8

        now = timezone.now()
        later = timezone.now() + timezone.timedelta(days=max_graph_days)

        data = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': '',
            'manual_initial_date': now,
            'manual_final_date': later
        }

        form = NewGraphForm(data)

        self.assertFalse(form.is_valid())

        self.assertEqual(
            [_('Período informado muito longo.')],
            form.non_field_errors()
        )

    def create_building(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone"
        )

        building = Building.objects.create(
            campus=campus,
            name="Test Building",
            server_ip_address="1.1.1.1"
        )

        return building
