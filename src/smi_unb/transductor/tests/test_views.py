import mock
import requests

from django.urls import reverse
from django.contrib.auth.models import User
from django.test.client import Client
from django.test import TestCase
from django.utils import timezone

from smi_unb.api.synchronization import SyncManager
from smi_unb.buildings.models import Building
from smi_unb.campuses.models import AdministrativeRegion, Campus
from smi_unb.users.models import UserPermissions
from smi_unb.transductor.forms import EnergyForm
from smi_unb.transductor.models import EnergyTransductor, TransductorModel


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse(None, 200)


class TransductorViewsTests(TestCase):
    def setUp(self):
        self.client = Client()

        self.user = User(
            username='test',
            email="test@test.com",
            is_superuser=True,
        )
        self.user.set_password('password')
        self.user.save()

        self.normaluser = User(
            username='normal_user',
            email="normal@test.com",
            is_superuser=False,
        )
        self.normaluser.set_password('12345')
        self.normaluser.save()

        self.building = self.create_building()

        self.t_model = TransductorModel.objects.create(
            name="TR 4020",
            transport_protocol="UDP",
            serial_protocol="Modbus RTU",
            register_addresses=[[68, 0], [70, 1]],
        )

        self.transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test',
            '1.1.1.1'
        )

    def test_transductor_pages_status_code_with_login(self):
        self.client.login(username='test@test.com', password='password')

        url_new = reverse(
            'transductor:new',
            kwargs={'building_id': self.building.id}
        )
        response_1 = self.client.get(url_new)
        self.assertEqual(200, response_1.status_code)

        transductor = self.transductor
        url_edit = reverse(
            'transductor:edit',
            kwargs={'transductor_id': transductor.id}
        )
        response_2 = self.client.get(url_edit)
        self.assertEqual(200, response_2.status_code)

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2'
        )
        url_detail = reverse(
            'transductor:info',
            kwargs={'transductor_id': transductor.id}
        )
        response_3 = self.client.get(url_detail)
        self.assertEqual(200, response_3.status_code)

    def test_transductor_pages_status_code_without_login(self):
        url_new = reverse(
            'transductor:new',
            kwargs={'building_id': self.building.id}
        )
        response_1 = self.client.get(url_new)
        self.assertEqual(302, response_1.status_code)

        transductor = self.transductor
        url_edit = reverse(
            'transductor:edit',
            kwargs={'transductor_id': transductor.id}
        )
        response_2 = self.client.get(url_edit)
        self.assertEqual(302, response_2.status_code)

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2'
        )
        url_detail = reverse(
            'transductor:info',
            kwargs={'transductor_id': transductor.id}
        )
        response_3 = self.client.get(url_detail)
        self.assertEqual(302, response_3.status_code)

    def test_not_create_energy_transductor_without_params(self):
        self.client.login(username='test@test.com', password='password')

        url = reverse(
            'transductor:new',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'name': '',
            'serie_number': '',
            'ip_address': '',
            'model': '',
            'building': ''
        }

        response = self.client.post(url, params)

        self.assertFormError(
            response, 'form', 'ip_address', 'This field is required.')
        self.assertFormError(
            response, 'form', 'model', 'This field is required.')
        self.assertFormError(
            response, 'form', 'building', 'This field is required.')

    @mock.patch.object(SyncManager, 'sync_transductor', return_value=True)
    @mock.patch.object(EnergyForm, 'is_valid', return_value=True)
    def test_create_valid_energy_transductor(self, mock_1, mock_2):
        self.client.login(username='test@test.com', password='password')

        transductor_count = EnergyTransductor.objects.count()

        url = reverse(
            'transductor:new',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'ip_address': '111.111.111.111',
            'model': self.t_model.id,
            'name': 'test',
            'building': self.building.id
        }

        response = self.client.post(url, params)

        mock_1.assert_called()
        mock_2.assert_called()

        self.assertEqual(
            transductor_count + 1,
            EnergyTransductor.objects.count())

        self.assertRedirects(
            response,
            reverse(
                'buildings:index',
                kwargs={'building_id': self.building.id}
            )
        )

    def test_not_create_transductor_with_same_ip_address(self):
        self.client.login(username='test@test.com', password='password')

        transductor = self.transductor

        url = reverse(
            'transductor:new',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'ip_address': transductor.ip_address,
            'name': 'test',
            'model': self.t_model.id,
            'building': self.building.id
        }

        response = self.client.post(url, params)

        self.assertFormError(
            response,
            'form',
            'ip_address',
            'Transductor with this Ip address already exists.'
        )

    def test_not_create_transductor_with_wrong_ip_address(self):
        self.client.login(username='test@test.com', password='password')

        url = reverse(
            'transductor:new',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'ip_address': '1',
            'name': 'test',
            'model': self.t_model.id,
            'building': self.building.id
        }

        response = self.client.post(url, params)

        self.assertFormError(
            response, 'form', 'ip_address', 'Incorrect IP address format')

    @mock.patch.object(SyncManager, 'sync_transductor', return_value=True)
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_change_transductor_model(self, mock_get, mock_sync):
        self.client.login(username='test@test.com', password='password')

        start_date = timezone.now() \
                             .replace(second=00, microsecond=00)

        transductor = self.create_energy_transductor(
            building=self.building,
            model=self.t_model,
            ip_address='2.2.2.2',
            name='transductor test 2',
            serie_number=1,
            local_description='Test Local',
            comments='Test Comment',
            calibration_date=start_date
        )

        t_model_2 = TransductorModel.objects.create(
            name="Transductor Model 2",
            transport_protocol="TCP/IP",
            serial_protocol="Mosbus",
            register_addresses=[[100, 0], [105, 1]]
        )

        url = reverse(
            'transductor:edit',
            kwargs={'transductor_id': transductor.id}
        )

        new_date = timezone.now() - timezone.timedelta(minutes=9)
        new_date = new_date.replace(second=00, microsecond=00)

        params = {
            'building': self.building.id,
            'model': t_model_2.id,
            'ip_address': '9.9.9.9',
            'name': 'test',
            'serie_number': 9,
            'local_description': 'Test Local 9',
            'comments': 'Test Comment 9',
            'calibration_date': new_date.strftime('%d/%m/%Y %H:%M')
        }

        self.client.post(url, params)

        transductor = EnergyTransductor.objects.get(
            ip_address='9.9.9.9'
        )

        mock_get.assert_called()
        mock_sync.assert_called()
        self.assertEqual(t_model_2, transductor.model)
        self.assertEqual('9.9.9.9', transductor.ip_address)
        self.assertEqual('Test Local 9', transductor.local_description)
        self.assertEqual('Test Comment 9', transductor.comments)
        self.assertEqual(new_date, transductor.calibration_date)
        self.assertEqual(True, transductor.synchronized)

    def test_not_edit_transductor_without_changes(self):
        self.client.login(username='test@test.com', password='password')

        url = reverse(
            'transductor:edit',
            kwargs={'transductor_id': self.transductor.id}
        )

        params = {
            'building': self.building.id,
            'model': self.t_model.id,
            'name': self.transductor.name,
            'ip_address': self.transductor.ip_address,
        }

        response = self.client.post(url, params)

        self.assertFormError(
            response, 'form', None, 'Nenhuma Mudança Encontrada.')

    def test_not_edit_transductor_with_wrong_params(self):
        self.client.login(username='test@test.com', password='password')

        url = reverse(
            'transductor:edit',
            kwargs={'transductor_id': self.transductor.id}
        )

        params = {
            'building': self.building.id,
            'model': self.t_model.id,
            'name': 'test',
            'ip_address': 'Wrong Ip Addres',
        }

        response = self.client.post(url, params)

        self.assertFormError(
            response, 'form', 'ip_address', 'Incorrect IP address format')

    def test_energy_transductor_detail(self):
        self.client.login(username='test@test.com', password='password')

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2'
        )

        url = reverse(
            'transductor:info',
            kwargs={'transductor_id': transductor.id}
        )

        response = self.client.get(url)

        self.assertIn(
            self.building.name, response.content.decode('utf-8'))

    @mock.patch.object(SyncManager, 'sync_transductor', return_value=False)
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_enable_energy_transductor(self, mock_get, mock_sync):
        UserPermissions.add_user_permissions(
            self.normaluser, ['manager_transductors'])

        self.client.login(username='normal@test.com', password='12345')

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2',
            active=False
        )

        url = reverse(
            'transductor:enable', kwargs={'transductor_id': transductor.id}
        )

        params = {
            'next': ''
        }

        response = self.client.post(url, params)

        enabled_transductor = EnergyTransductor.objects.get(id=transductor.id)

        self.assertEqual(True, enabled_transductor.active)
        self.assertEqual(False, enabled_transductor.synchronized)

        next_page = reverse(
            'transductor:info',
            kwargs={'transductor_id': transductor.id}
        )

        mock_get.assert_called()
        mock_sync.assert_called()
        self.assertRedirects(response, next_page)

    @mock.patch.object(SyncManager, 'sync_transductor', return_value=True)
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_enable_energy_transductor_with_next_param(self, mock_1, mock_2):
        UserPermissions.add_user_permissions(
            self.normaluser, ['manager_transductors'])

        self.client.login(username='normal@test.com', password='12345')

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2',
            active=False
        )

        url = reverse(
            'transductor:enable', kwargs={'transductor_id': transductor.id}
        )

        next_page = reverse(
            'buildings:index', kwargs={'building_id': transductor.building.id}
        )

        params = {
            'next': next_page
        }

        response = self.client.post(url, params)

        enabled_transductor = EnergyTransductor.objects.get(id=transductor.id)

        mock_1.assert_called()
        mock_2.assert_called()
        self.assertEqual(True, enabled_transductor.active)
        self.assertEqual(True, enabled_transductor.synchronized)
        self.assertRedirects(response, next_page)

    @mock.patch('requests.get', side_effect=requests.ConnectionError('Error.'))
    def test_not_enable_energy_transductor_without_server_connection(
        self, mock_get
    ):
        UserPermissions.add_user_permissions(
            self.normaluser, ['manager_transductors'])

        self.client.login(username='normal@test.com', password='12345')

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2',
            active=False
        )

        url = reverse(
            'transductor:enable', kwargs={'transductor_id': transductor.id}
        )

        next_page = reverse(
            'buildings:index', kwargs={'building_id': transductor.building.id}
        )

        params = {
            'next': next_page
        }

        self.client.post(url, params)

        not_active_transductor = EnergyTransductor.objects.get(
            ip_address=transductor.ip_address)

        self.assertEqual(False, not_active_transductor.active)

    def test_not_enable_energy_transductor_get_method(self):
        UserPermissions.add_user_permissions(
            self.normaluser, ['manager_transductors'])

        self.client.login(username='normal@test.com', password='12345')

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2',
            active=False
        )

        url = reverse(
            'transductor:enable', kwargs={'transductor_id': transductor.id}
        )

        response = self.client.get(url)

        enabled_transductor = EnergyTransductor.objects.get(id=transductor.id)

        self.assertEqual(False, enabled_transductor.active)

        next_page = reverse(
            'transductor:info',
            kwargs={'transductor_id': transductor.id}
        )

        self.assertRedirects(response, next_page)

    @mock.patch.object(SyncManager, 'sync_transductor', return_value=True)
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_disable_energy_transductor(self, mock_get, mock_sync):
        UserPermissions.add_user_permissions(
            self.normaluser, ['manager_transductors'])

        self.client.login(username='normal@test.com', password='12345')

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2',
            active=True
        )

        url = reverse(
            'transductor:disable', kwargs={'transductor_id': transductor.id}
        )

        params = {
            '': ''
        }

        self.client.post(url, params)

        disabled_transductor = EnergyTransductor.objects.get(id=transductor.id)

        mock_get.assert_called()
        mock_sync.assert_called()
        self.assertEqual(False, disabled_transductor.active)
        self.assertEqual(True, disabled_transductor.synchronized)

    @mock.patch('requests.get', side_effect=requests.ConnectionError('Error.'))
    def test_not_disable_energy_transductor_without_server_connection(
        self, mock_get
    ):
        UserPermissions.add_user_permissions(
            self.normaluser, ['manager_transductors'])

        self.client.login(username='normal@test.com', password='12345')

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2',
            active=True
        )

        url = reverse(
            'transductor:disable', kwargs={'transductor_id': transductor.id}
        )

        params = {
            '': ''
        }

        self.client.post(url, params)

        not_disabled_transductor = EnergyTransductor.objects.get(
            id=transductor.id)

        mock_get.assert_called()
        self.assertEqual(True, not_disabled_transductor.active)

    def test_not_disable_energy_transductor_with_get_method(self):
        self.client.login(username='test@test.com', password='password')

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2',
            active=True
        )

        url = reverse(
            'transductor:disable',
            kwargs={'transductor_id': transductor.id}
        )

        self.client.get(url)

        self.assertEqual(True, transductor.active)

    def test_not_enable_energy_transductor_with_get_method(self):
        self.client.login(username='test@test.com', password='password')

        transductor = self.create_energy_transductor(
            self.building,
            self.t_model,
            'transductor test 2',
            '2.2.2.2',
            active=False
        )

        url = reverse(
            'transductor:disable',
            kwargs={'transductor_id': transductor.id}
        )

        self.client.get(url)

        self.assertEqual(False, transductor.active)

    def create_building(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone",
            url_param="test_campus"
        )

        building = Building.objects.create(
            campus=campus,
            name="Test Building",
            server_ip_address="1.1.1.1"
        )

        return building

    def create_energy_transductor(
        self, building, model, name, ip_address, serie_number=None,
        local_description='', comments='', calibration_date=None,
        active=False
    ):
        transductor = EnergyTransductor.objects.create(
            building=building,
            model=model,
            ip_address=ip_address,
            name=name,
            serie_number=serie_number,
            local_description=local_description,
            comments=comments,
            calibration_date=calibration_date,
            active=active
        )

        return transductor
